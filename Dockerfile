FROM cern/cc7-base

MAINTAINER Andreas Wagner <Andreas.Wagner@cern.ch>

WORKDIR /usr/local/ezproxy
COPY docker-entrypoint.sh /entrypoint.sh


RUN yum install -y php5 curl lib32z1 dnsutils vim  \
    &&  yum install -y /lib/ld-linux.so.2  \
    && curl -sS -k https://www.oclc.org/content/dam/support/ezproxy/documentation/download/binaries/6-2-2/ezproxy-linux.bin > ./ezproxy \
    && chmod 755 ./ezproxy /entrypoint.sh


# EXPOSE 50162
EXPOSE 80
EXPOSE 443

ENTRYPOINT ["/entrypoint.sh"]

# ENTRYPOINT ["/bin/bash"]
